package br.com.caelum.notasfiscais.mb;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import br.com.caelum.notasfiscais.dao.UsuarioDao;
import br.com.caelum.notasfiscais.modelo.Usuario;
import br.com.caelum.notasfiscais.util.Model;

@Model
public class LoginBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Usuario usuario = new Usuario();
	
	@Inject
	private UsuarioDao usuarioDao;
	
	@Inject
	private UsuarioLogadoBean usuarioLogadoBean;
	
	@Inject
	private Event<Usuario> eventoLogin;
	
	public String efetuaLogin(){
		boolean loginValido = usuarioDao.existe(this.usuario);
		System.out.println("O login era valido? " + loginValido);
		if (loginValido){
			this.eventoLogin.fire(usuario);
			this.usuarioLogadoBean.logar(usuario);
			return "produto?faces-redirect=true";
		}
		this.usuarioLogadoBean.deslogar();
		this.usuario = new Usuario();
		return "login";
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public String logout(){
		this.usuarioLogadoBean.deslogar();
		return "login?faces-redirect=true";
	}
}
