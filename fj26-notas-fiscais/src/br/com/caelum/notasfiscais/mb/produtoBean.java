package br.com.caelum.notasfiscais.mb;


import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import br.com.caelum.notasfiscais.dao.ProdutoDao;
import br.com.caelum.notasfiscais.modelo.Produto;
import br.com.caelum.notasfiscais.tx.Transactional;
import br.com.caelum.notasfiscais.util.Model;

@Model
public class ProdutoBean {
	private Produto produto = new Produto();
	private List<Produto> produtos;
	private double precoTotal;
	
	@Inject
	private ProdutoDao produtoDao;
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	@Transactional
	public void grava(){
		if(this.produto.getId() == null){
			produtoDao.adiciona(produto);
		}else{
			produtoDao.atualiza(produto);
		}
		
		produto = new Produto();
		this.produtos = produtoDao.listaTodos();
	}

	public List<Produto> getProdutos(){
		if (produtos == null){
			System.out.println("Carregando produtos...");
			produtos = produtoDao.listaTodos();
		}
		
		
		return produtos;
	}
	
	public double getPrecoTotal(){
		for(Produto produto: produtos){
			this.precoTotal += produto.getPreco();
		}
		return precoTotal;
	}
	
	@Transactional
	public void remove(Produto produto){
		produtoDao.remove(produto);
		this.produtos = produtoDao.listaTodos();
	}
	
	public void cancelarAEdicao(){
		System.out.println("Raaawwwr!");
		this.produto = new Produto();
	}
	
	public void comecaComMaiuscula(FacesContext fc, UIComponent component, Object value) throws ValidatorException{
		String valor = value.toString();
		
		if(!valor.matches("[A-Z].*")){
			throw new ValidatorException(new FacesMessage("Deveria começar com maiúscula"));
		}
		
	}
}
