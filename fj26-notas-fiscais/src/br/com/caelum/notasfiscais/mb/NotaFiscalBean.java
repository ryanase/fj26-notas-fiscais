package br.com.caelum.notasfiscais.mb;

import java.io.Serializable;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.notasfiscais.dao.NotaFiscalDao;
import br.com.caelum.notasfiscais.dao.ProdutoDao;
import br.com.caelum.notasfiscais.modelo.Item;
import br.com.caelum.notasfiscais.modelo.NotaFiscal;
import br.com.caelum.notasfiscais.modelo.Produto;
import br.com.caelum.notasfiscais.tx.Transactional;

@Named
@ConversationScoped
public class NotaFiscalBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private NotaFiscal notaFiscal = new NotaFiscal();
	private Item item = new Item();
	private long idProduto;
	
	@Inject
	private NotaFiscalDao notaFiscalDao;
	
	@Inject
	private ProdutoDao produtoDao;
	
	@Inject
	private Conversation conversation;
	
	@Transactional
	public String gravar(){
		this.notaFiscalDao.adiciona(notaFiscal);
		this.notaFiscal = new NotaFiscal();
		this.conversation.end();
		return ("notafiscal?faces-redirect=true");
	}
	
	public void guardaItem(){
		Produto produto = produtoDao.buscaPorId(idProduto);
		item.setProduto(produto);
		item.setValorUnitario(produto.getPreco());
		
		notaFiscal.getItens().add(item);
		item.setNotaFiscal(notaFiscal);
		
		item = new Item();
		idProduto = 1;
	}
	
	public String avancar(){
		if (this.conversation.isTransient()){
			this.conversation.begin();
		}
		return "item?faces-redirect=true";
	}
	
	//Getters And Setters
	public NotaFiscal getNotaFiscal(){
		return this.notaFiscal;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(long idProduto) {
		this.idProduto = idProduto;
	}
}
