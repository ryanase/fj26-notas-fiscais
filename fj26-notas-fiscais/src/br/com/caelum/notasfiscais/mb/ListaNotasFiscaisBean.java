package br.com.caelum.notasfiscais.mb;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.notasfiscais.dao.NotaFiscalDao;
import br.com.caelum.notasfiscais.datamodel.DataModelNotasFiscais;
import br.com.caelum.notasfiscais.modelo.NotaFiscal;
import br.com.caelum.notasfiscais.util.ViewModel;

@ViewModel
public class ListaNotasFiscaisBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<NotaFiscal> notasFiscais;
	
	@Inject
	private NotaFiscalDao notaFiscalDao;
	
	@Inject
	private DataModelNotasFiscais dataModel;
	
	public List<NotaFiscal> getNotasFiscais() {
		if(this.notasFiscais == null){
			this.notasFiscais = notaFiscalDao.listaTodos();
		}
		return notasFiscais;
	}

	public DataModelNotasFiscais getDataModel() {
		return dataModel;
	}
	
	
}
