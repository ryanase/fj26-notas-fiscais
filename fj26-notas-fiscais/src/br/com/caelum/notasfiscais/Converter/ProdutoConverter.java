package br.com.caelum.notasfiscais.Converter;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.caelum.notasfiscais.dao.ProdutoDao;
import br.com.caelum.notasfiscais.modelo.Produto;

@RequestScoped
@FacesConverter(forClass=br.com.caelum.notasfiscais.modelo.Produto.class)
public class ProdutoConverter implements Converter{
	
	@Inject
	private ProdutoDao produtoDao;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return produtoDao.buscaPorId(Long.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Produto produto = (Produto) value;
		return String.valueOf(produto.getId());
	}
	
}
